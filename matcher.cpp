#include <iostream>
#include "pairmatcher.h"

int main() {
    PairMatcher matcher('(', ')');
    
    string testString = "((())()";
    
    cout << testString << " is " << (matcher.check(testString) ? "valid" : "invalid") << endl;
    return 0;
}
