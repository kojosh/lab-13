#include "charstack.h"

CharStack::CharStack(int max) {
    maxSize = max;
    stack = new char[maxSize];
}
CharStack::~CharStack() {
    delete [] stack;
}
bool CharStack::push(char c) {
    if(size == maxSize) {
        return false;
    }
    stack[size] = c;
    size++;
    return true;
}
char CharStack::pop() {
    if(size == 0) {
        return 0;
    }
    return stack[--size];
}
bool CharStack::isEmpty() const {
    return size == 0;
}