#ifndef CHARSTACK_H
#define CHARSTACK_H
#include <iostream>
using namespace std;

class CharStack {
private:
    char *stack;
    int maxSize;
    int size = 0;
public:
    CharStack(int max);
    ~CharStack();
    bool push(char c);
    char pop();
    bool isEmpty() const;
};

#endif