#pragma once
#include <iostream>
#include "charstack.h"
using namespace std;

class PairMatcher {
private:
    char _openChar, _closeChar;
    CharStack charStack;
public:
    PairMatcher(char openChar, char closeChar) : charStack(100) {}
    bool check(const string &testString);
};